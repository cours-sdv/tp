package fr.supdevinci.bdd.b3.bddb3.tp.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LivreRowMapper implements RowMapper<Livre>{
    @Override
    public Livre mapRow(ResultSet rs, int rowNum) throws SQLException {
        Livre livre = new Livre();
        livre.setId(rs.getLong("id"));
        livre.setAuteur(rs.getString("auteur"));
        livre.setTitre(rs.getString("titre"));
        livre.setAnneeParution(rs.getInt("annee_parution"));

        return livre;
    }
}
