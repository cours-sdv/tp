package fr.supdevinci.bdd.b3.bddb3.tp.service;

import fr.supdevinci.bdd.b3.bddb3.tp.model.Livre;
import fr.supdevinci.bdd.b3.bddb3.tp.model.LivreRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class LivreService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int addLivre(Livre livre){
        return jdbcTemplate.update("INSERT into tp.livre (titre, auteur, annee_parution) VALUES (?, ?, ?)", livre.getTitre(), livre.getAuteur(), livre.getAnneeParution());
    }

    public int updateLivre(Livre livre){
        return jdbcTemplate.update("UPDATE tp.livre SET titre = ?, auteur = ?, annee_parution = ? WHERE id = ?", livre.getTitre(), livre.getAuteur(), livre.getAnneeParution(), livre.getId());
    }

    public List<Livre> getLivresByTitreOrAuteur(String souschaineDuTitreOuDeAuteur){
        List<Livre> listLivres = jdbcTemplate.query("select * from tp.livre where titre like ? or auteur like ?", new LivreRowMapper(), souschaineDuTitreOuDeAuteur, souschaineDuTitreOuDeAuteur);
        return listLivres;
    }

    public List<Livre> getLivresByAnneeParution(int anneeParution){
        String sql = "SELECT * FROM tp.livre WHERE annee_parution =  ?";
        return jdbcTemplate.query(sql, new LivreRowMapper(), anneeParution);
    }

    public Livre getLivreById(Long idLivre){
        String sql = "SELECT * FROM tp.livre WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new LivreRowMapper(), idLivre);
    }

    public int deleteLivreById(Long idLivre){
        String deleteQuery = "delete from tp.livre where id = ?";
        return jdbcTemplate.update(deleteQuery, idLivre);
    }
}
