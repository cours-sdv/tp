package fr.supdevinci.bdd.b3.bddb3.tp.repository;

import fr.supdevinci.bdd.b3.bddb3.tp.model.Classe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClasseRepository extends JpaRepository<Classe, Long> {
    @Query("SELECT count(c) FROM Classe c")
    int getNbClasses();
}
