package fr.supdevinci.bdd.b3.bddb3.tp;

import fr.supdevinci.bdd.b3.bddb3.tp.model.Livre;
import fr.supdevinci.bdd.b3.bddb3.tp.service.ClasseService;
import fr.supdevinci.bdd.b3.bddb3.tp.service.LivreService;
import fr.supdevinci.bdd.b3.bddb3.tp.service.SpecialiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MainTest {

    @Autowired
    private ClasseService classeService;

    @Autowired
    private LivreService livreService;

    @Autowired
    private SpecialiteService SpecialiteService;


    @PostConstruct
    void execute() {
        System.out.println("demarre");

        System.out.println(livreService.getLivresByTitreOrAuteur("auteur"));
        System.out.println("termine");
    }
}
