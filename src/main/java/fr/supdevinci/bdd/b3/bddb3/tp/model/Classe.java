package fr.supdevinci.bdd.b3.bddb3.tp.model;

import javax.persistence.*;

@Entity
@Table(name = "classe", schema = "tp")
public class Classe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer anne;

    @ManyToOne
    @JoinColumn(name = "specialite_id")
    private Specialite specialite;

    public Classe(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAnne() {
        return anne;
    }

    public void setAnne(Integer anne) {
        this.anne = anne;
    }

    public Specialite getSpecialite() {
        return specialite;
    }

    public void setSpecialite(Specialite specialite) {
        this.specialite = specialite;
    }
}
