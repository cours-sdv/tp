package fr.supdevinci.bdd.b3.bddb3.tp.repository;

import fr.supdevinci.bdd.b3.bddb3.tp.model.Specialite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SpecialiteRepository extends JpaRepository<Specialite, Long> {
    @Query("SELECT count(s) FROM Specialite s")
    int getNbSpecialite();
}
