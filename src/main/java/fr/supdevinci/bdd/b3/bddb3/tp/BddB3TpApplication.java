package fr.supdevinci.bdd.b3.bddb3.tp;

import fr.supdevinci.bdd.b3.bddb3.tp.service.ClasseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringBootApplication
public class BddB3TpApplication {

	public static void main(String[] args) {
		SpringApplication.run(BddB3TpApplication.class, args);
	}

}
