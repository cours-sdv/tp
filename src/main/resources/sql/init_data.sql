
delete from tp.livre_specialite;
delete from tp.classe;
delete from tp.livre;
delete from tp.specialite;

insert into tp.specialite(domaine, libelle) values ('Divertissement', 'Spectacle vivant');
insert into tp.specialite(domaine, libelle) values ('Divertissement', 'Théâtre');
insert into tp.specialite(domaine, libelle) values ('Investissement', 'Trading');
insert into tp.specialite(domaine, libelle) values ('Informatique', 'Analyse');
insert into tp.specialite(domaine, libelle) values ('Informatique', 'Test');
insert into tp.specialite(domaine, libelle) values ('Divers', 'Géopolitique');

insert into tp.livre(titre, auteur, annee_parution) values ('L''art de la fuite', 'Guizmo', 1990);
insert into tp.livre(titre, auteur, annee_parution) values ('Le champigon gelé', 'Guizmo', 2000);
insert into tp.livre(titre, auteur, annee_parution) values ('Le jardin du château', 'Guizmo', 2000);
insert into tp.livre(titre, auteur, annee_parution) values ('Comprendre les BDD', 'Molière Le retour', 2012);
insert into tp.livre(titre, auteur, annee_parution) values ('Mécanique', 'Molière Le retour', 2012);
insert into tp.livre(titre, auteur, annee_parution) values ('L''air du temps', 'Molière Le retour', 2018);
insert into tp.livre(titre, auteur, annee_parution) values ('Réussir sa vie financière', 'Inconnu 1', 2016);
insert into tp.livre(titre, auteur, annee_parution) values ('Le week-end', 'Inconnu 2', 2013);

insert into tp.classe (annee, specialite_id) select 2022, id from tp.specialite where libelle='Spectacle vivant';
insert into tp.classe (annee, specialite_id) select 2021, id from tp.specialite where libelle='Test';
insert into tp.classe (annee, specialite_id) select 2020, id from tp.specialite where libelle='Analyse';
insert into tp.classe (annee, specialite_id) select 2019, id from tp.specialite where libelle='Trading';
insert into tp.classe (annee, specialite_id) select 2008, id from tp.specialite where libelle='Trading';
insert into tp.classe (annee, specialite_id) select 2007, id from tp.specialite where libelle='Trading';

insert into tp.livre_specialite(livre_id, specialite_id) select l.id, s.id from tp.livre l, tp.specialite s
where (l.titre, s.libelle) in (
    ('Le jardin du château', 'Spectacle vivant'),
    ('Le jardin du château', 'Théâtre'),
    ('Mécanique', 'Analyse'),
    ('Mécanique', 'Test'),
    ('Le champigon gelé', 'Spectacle vivant'),
    ('Le champigon gelé', 'Géopolitique'),
    ('L''art de la fuite', 'Géopolitique'),
    ('Comprendre les BDD', 'Analyse'),
    ('Comprendre les BDD', 'Test'),
    ('Le week-end', 'Spectacle vivant'),
    ('Le week-end', 'Géopolitique'),
    ('Le week-end', 'Réussir sa vie financière'));

