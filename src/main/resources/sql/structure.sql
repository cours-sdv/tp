
create table tp.classe (
   id serial,
   annee smallint,
   specialite_id bigint
);

create table tp.specialite(
   id serial,
   domaine varchar(30),
   libelle varchar(100)
);

create table tp.livre(
  id serial,
  titre varchar(30),
  auteur varchar(100),
  annee_parution smallint
);

create table tp.livre_specialite(
    livre_id bigint,
    specialite_id bigint
);

-- Clés primaires
alter table tp.livre add constraint livre_pk primary key (id);
alter table tp.classe add constraint classe_pk primary key (id);
alter table tp.specialite add constraint specialite_pk primary key (id);
alter table tp.livre_specialite add constraint livre_specialite_pk primary key (livre_id, specialite_id);

-- Clés étrangères
alter table tp.classe add constraint classe_specialite_fk foreign key (specialite_id) references tp.specialite(id);

alter table tp.livre_specialite add constraint livre_specialite_1_fk foreign key (specialite_id) references tp.specialite(id);
alter table tp.livre_specialite add constraint livre_specialite_2_fk foreign key (livre_id) references tp.livre(id);
