# TP

## Installation

Vous trouverez dans `src/main/resources/structure.sql` le script SQL de création d'un schéma et des ses tables (PostgreSQL).
Le fichier `src/main/resources/init_data.sql` contient le script SQL d'initialisation des données : vous pouvez le rejouer à loisir.

Exécuter le contenu de ce script SQL depuis votre client SQL.

## Informations

BddB3TpApplication est la classe de lancement de l'application.
Le point d'entrée de votre code se situe dans la méthode **execute** de la classe **MainTest**.

Les tables sont les suivantes :
- classe(**id**, annee, #specialite_id);
- specialite(**id**, domaine, libelle);
- livre(**id**, titre, auteur, annee_parution);

Les **clés primaires** sont en gras et la clé étrangère #specialite_id référence le champ id de la table specialite.


## Exercices

### Entités (3 pts)

Faire en sorte que les classes suivantes correspondent aux entités des tables :
 - specialite
 - classe
 - livre

 Penser à respecter les contraintes sur les colonnes.

### JDBC Template (1.5pts)

Dans la classe LivreService, utiliser JdbcTemplate pour créer les méthodes :
- public void addLivre(Livre livre)
- public void updateLivre(Livre livre)
- public List<Livre> getLivresByTitreOrAuteur(Livre livre, String souschaineDuTitreOuDeAuteur)
- public List<Livre> getLivresByTitreOrAuteur(Livre livre, int anneeParution)
- public Livre getLivreById(Long idLivre)
- public void deleteLivreById(Long idLivre)

### Entités (1.5pts)

**/!\ Ne vous occupez pas des relations entre les entités**

Transformez en entités les classes suivantes :
- Classe
- Livre
- Specialite

Pensez à préciser les informations dont vous disposez pour minimiser le nombre d'erreurs détectées par la base de données.

### Entités (0.5pts)

Ecrire la relation entre les entités Classe et Specialite sachant que :
 - une Specialite peut être associée à plusieurs Classe 
 - il n'est pas utile de récupérer systématiquement les classes depuis une spécialité
 - il n'est pas utile de récupérer systématiquement les spécialités depuis une classe
 - la suppression d'une classe ou d'une spécialité sont des opérations isolées et indépendantes


### Repository JPA (1.5pts)

Créer les repositories JPA pour les entités suivantes :
 - Classe (ClasseRepository)
 - Specialite (SpecialiteRepository)

Ecrire dans ces repositories des méthodes avec des requêtes natives pour récupérer :
 - le nombre de classes (méthode : int getNbClasses()),
 - le nombre de spécialités (méthode : int getNbSpecialites()),

Ecrire dans ces repositories des méthodes avec des requêtes JPQL pour récupérer :
 - Les livres comportant une sous-chaine de caractères dans l'auteur et/ou le titre (une seule expression pour les deux critères et sans tenir compte de la casse) 
 - Les classes qui correspondent à une liste d'années passée en paramètre via une liste

### Association plusieurs à plusieurs (1pt)

Mettez en place la relation entre les entités Livre et Specialite sachant que :
 - la suppression d'un livre ne provoque pas la suppression des spécialités associées
 - la suppression d'une spécialité ne provoque pas la suppression des livres associés

Vérifier avec une nouvelle méthode testLivreSpecialite dans la classe MainTest qu'il est possible de :
 - créer ces associations à partir de l'entité Livre
 - de supprimer l'association livre-entité depuis l'entité Livre

### Vérification (1pt)
Vérifier dans une nouvelle méthode testClasseComplet de la classe MainTest qu'il est possible de :
 - procéder à l'ajout d'une classe
 - afficher la classe
 - mettre à jour la classe
 - afficher la classe modifiée
 - supprimer la classe

Procéder de même pour les livres et les spécialités à partir de leurs services respectifs (méthodes testLivreComplet, testSpecialiteComplet)
